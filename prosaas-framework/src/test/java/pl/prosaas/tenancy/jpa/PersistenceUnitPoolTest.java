package pl.prosaas.tenancy.jpa;

import com.google.common.base.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManagerFactory;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceUnitPoolTest {
    private final static String TENANT_ID = "The Tenant";

    @Mock
    private EntityManagerFactory mockFactory;

    private PersistenceUnitPool persistenceUnitPool;

    @Before
    public void setUp() {
        persistenceUnitPool = new PersistenceUnitPool("unit-name");
    }

    @Test
    public void testSetEntityManagerFactoryTenantAware() {
        assertThat(persistenceUnitPool.getEntityManagerFactory(TENANT_ID)).isEqualTo(Optional.absent());

        persistenceUnitPool.setEntityManagerFactory(TENANT_ID, mockFactory);
        assertThat(persistenceUnitPool.getEntityManagerFactory(TENANT_ID).get()).isEqualTo(mockFactory);
    }

    @Test
    public void testSetEntityManagerFactoryCrossTenant() {
        assertThat(persistenceUnitPool.getEntityManagerFactory()).isEqualTo(Optional.absent());

        persistenceUnitPool.setEntityManagerFactory(mockFactory);
        assertThat(persistenceUnitPool.getEntityManagerFactory().get()).isEqualTo(mockFactory);
    }
}
