package pl.prosaas.tenancy.jpa;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManagerFactory;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EntityManagerFactoryHolderTest {
    private final static String UNIT_A = "unit-a";

    private final static String TENANT_ID = "The Tenant";

    private EntityManagerFactoryHolder factoryHolder;

    @Mock
    private EntityManagerFactory factory;

    @Mock
    private EntityManagerFactory otherFactory;

    @Mock
    private PersistenceUnitPool persistenceUnitPool;

    @Before
    public void setUp() {
        factoryHolder = new EntityManagerFactoryHolder();
        when(persistenceUnitPool.getName()).thenReturn(UNIT_A);
    }

    @Test
    public void getTenantFactoryPoolAbsent() {
        assertThat(factoryHolder.getEntityManagerFactory(UNIT_A, TENANT_ID)).isEqualTo(Optional.absent());
    }

    @Test
    public void getTenantFactoryPoolPresentFactoryAbsent() {
        when(persistenceUnitPool.getEntityManagerFactory(TENANT_ID)).thenReturn(Optional.<EntityManagerFactory>absent());
        factoryHolder.setPersistenceUnitPools(mockPoolsList());
        assertThat(factoryHolder.getEntityManagerFactory(UNIT_A, TENANT_ID)).isEqualTo(Optional.absent());
    }

    @Test
    public void getTenantFactoryPresent() {
        when(persistenceUnitPool.getEntityManagerFactory(TENANT_ID)).thenReturn(Optional.of(factory));
        factoryHolder.setPersistenceUnitPools(mockPoolsList());
        assertThat(factoryHolder.getEntityManagerFactory(UNIT_A, TENANT_ID).get()).isEqualTo(factory);
    }

    @Test
    public void getCrossTenantFactoryPoolAbsent() {
        assertThat(factoryHolder.getEntityManagerFactory(UNIT_A)).isEqualTo(Optional.absent());
    }

    @Test
    public void getCrossTenantFactoryPoolPresentFactoryAbsent() {
        when(persistenceUnitPool.getEntityManagerFactory()).thenReturn(Optional.<EntityManagerFactory>absent());
        factoryHolder.setPersistenceUnitPools(mockPoolsList());
        assertThat(factoryHolder.getEntityManagerFactory(UNIT_A)).isEqualTo(Optional.absent());
    }

    @Test
    public void getCrossTenantFactoryPresent() {
        when(persistenceUnitPool.getEntityManagerFactory()).thenReturn(Optional.of(factory));
        factoryHolder.setPersistenceUnitPools(mockPoolsList());
        assertThat(factoryHolder.getEntityManagerFactory(UNIT_A).get()).isEqualTo(factory);
    }

    @Test
    public void testSetEntityManagerFactoryTenantAware() {
        factoryHolder.setEntityManagerFactory(UNIT_A, TENANT_ID, factory);
        assertThat(factoryHolder.getEntityManagerFactory(UNIT_A, TENANT_ID).get()).isEqualTo(factory);
    }

    @Test
    public void testSetEntityManagerFactoryCrossTenant() {
        factoryHolder.setEntityManagerFactory(UNIT_A, factory);
        assertThat(factoryHolder.getEntityManagerFactory(UNIT_A).get()).isEqualTo(factory);
    }

    @Test
    public void testSetMultipleFactoryManagers() {
        factoryHolder.setEntityManagerFactory(UNIT_A, factory);
        factoryHolder.setEntityManagerFactory(UNIT_A, TENANT_ID, otherFactory);
        assertThat(factoryHolder.getPersistenceUnitPools()).hasSize(1);
    }

    private List<PersistenceUnitPool> mockPoolsList() {
        return Lists.newArrayList(new PersistenceUnitPool("fake-1"),
                persistenceUnitPool,
                new PersistenceUnitPool("fake-2"));
    }
}
