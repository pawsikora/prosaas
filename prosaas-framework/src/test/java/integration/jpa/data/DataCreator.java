package integration.jpa.data;

import integration.jpa.model.Employee;
import integration.jpa.model.Shared;

public class DataCreator {
    public static Employee createEmployee(String name, String role) {
        Employee employee = new Employee();
        employee.setName(name);
        employee.setRole(role);
        return employee;
    }

    public static Shared createShared(String data) {
        Shared shared = new Shared();
        shared.setData(data);
        return shared;
    }
}
