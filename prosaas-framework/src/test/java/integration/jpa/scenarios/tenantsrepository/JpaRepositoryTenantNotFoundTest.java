package integration.jpa.scenarios.tenantsrepository;

import org.junit.Test;
import pl.prosaas.tenancy.jpa.repository.JpaTenantsRepository;
import pl.prosaas.tenancy.repository.TenantException;
import pl.prosaas.tenancy.repository.TenantsRepository;

public class JpaRepositoryTenantNotFoundTest {
    private final static String TENANT_ID = "any tenant";

    private TenantsRepository tenantsRepository = new JpaTenantsRepository();

    @Test(expected = TenantException.TenantNotFoundException.class)
    public void testTenantNotFoundWhileGetting() throws Exception {
        tenantsRepository.findTenant(TENANT_ID);
    }

    @Test(expected = TenantException.TenantNotFoundException.class)
    public void testTenantNotFoundWhileActivating() throws Exception {
        tenantsRepository.activateTenant(TENANT_ID);
    }

    @Test(expected = TenantException.TenantNotFoundException.class)
    public void testTenantNotFoundWhileDeactivating() throws Exception {
        tenantsRepository.deactivateTenant(TENANT_ID);
    }
}
