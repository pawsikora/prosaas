package integration.jpa.scenarios.tenantsrepository;

import org.junit.Test;
import pl.prosaas.tenancy.jpa.repository.JpaTenantsRepository;
import pl.prosaas.tenancy.repository.TenantException;
import pl.prosaas.tenancy.repository.TenantsRepository;

public class JpaRepositoryTenantExistsTest {
    private final static String TENANT_ID = "the tenant";

    private TenantsRepository tenantsRepository = new JpaTenantsRepository();

    @Test(expected = TenantException.TenantAlreadyExistsException.class)
    public void testTenantAlreadyExists() throws Exception {
        tenantsRepository.addNewTenant(TENANT_ID);

        // second insert expected to fail
        tenantsRepository.addNewTenant(TENANT_ID);
    }
}
