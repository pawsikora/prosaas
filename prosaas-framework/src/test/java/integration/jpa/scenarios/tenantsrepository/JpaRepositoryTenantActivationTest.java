package integration.jpa.scenarios.tenantsrepository;

import org.junit.Test;
import pl.prosaas.tenancy.jpa.repository.JpaTenantsRepository;
import pl.prosaas.tenancy.repository.TenantsRepository;

import static org.fest.assertions.Assertions.assertThat;

public class JpaRepositoryTenantActivationTest {
    private final static String TENANT_ID = "the tenant";

    private TenantsRepository tenantsRepository = new JpaTenantsRepository();

    @Test
    public void testTenantActivityUpdate() throws Exception {
        tenantsRepository.addNewTenant(TENANT_ID);
        assertThat(tenantsRepository.findTenant(TENANT_ID).isActive()).isTrue();
        tenantsRepository.deactivateTenant(TENANT_ID);
        assertThat(tenantsRepository.findTenant(TENANT_ID).isActive()).isFalse();
        tenantsRepository.activateTenant(TENANT_ID);
        assertThat(tenantsRepository.findTenant(TENANT_ID).isActive()).isTrue();
    }
}
