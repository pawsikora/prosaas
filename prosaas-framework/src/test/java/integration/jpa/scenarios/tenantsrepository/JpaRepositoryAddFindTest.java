package integration.jpa.scenarios.tenantsrepository;

import org.junit.Test;
import pl.prosaas.tenancy.jpa.repository.JpaTenantsRepository;
import pl.prosaas.tenancy.repository.Tenant;
import pl.prosaas.tenancy.repository.TenantsRepository;

import static org.fest.assertions.Assertions.assertThat;

public class JpaRepositoryAddFindTest {
    private final static String TENANT_ID = "the tenant";
    private TenantsRepository tenantsRepository = new JpaTenantsRepository();

    @Test
    public void testAddFindTenant() throws Exception {
        Tenant tenant = tenantsRepository.addNewTenant(TENANT_ID);
        assertThat(tenant.getJoinDate()).isNotNull();
        assertThat(tenant.getId()).isEqualTo(TENANT_ID);
        assertThat(tenant.isActive()).isTrue();

        Tenant persistedTenant = tenantsRepository.findTenant(TENANT_ID);
        assertThat(persistedTenant.getId()).isEqualTo(tenant.getId());
        assertThat(persistedTenant.getJoinDate()).isEqualTo(tenant.getJoinDate());
        assertThat(persistedTenant.isActive()).isEqualTo(tenant.isActive());
    }
}
