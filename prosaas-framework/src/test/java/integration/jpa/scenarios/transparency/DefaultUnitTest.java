package integration.jpa.scenarios.transparency;

import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;

import static integration.jpa.TestConstants.MARS_TENANT_ID;
import static integration.jpa.TestConstants.SATURN_TENANT_ID;

public class DefaultUnitTest extends SpacesTransparencyTest {
    @Test
    public void testTransparencyOnDefaultUnit() {
        mockTenantId(SATURN_TENANT_ID);
        EntityManager saturnEntityManager = JpaStorage.getTenantAwareEntityManager();

        mockTenantId(MARS_TENANT_ID);
        EntityManager marsEntityManager = JpaStorage.getTenantAwareEntityManager();

        testTransparency(marsEntityManager, saturnEntityManager);
    }
}
