package integration.jpa.scenarios.transparency;

import integration.backend.templates.MockedTenancyTest;
import integration.jpa.dao.Dao;
import integration.jpa.model.Employee;

import javax.persistence.EntityManager;
import java.util.List;

import static integration.jpa.data.DataCreator.createEmployee;
import static org.fest.assertions.Assertions.assertThat;

public class SpacesTransparencyTest extends MockedTenancyTest {

    protected void testTransparency(EntityManager marsEntityManager, EntityManager saturnEntityManager) {
        Dao<Employee> marsEmployeeDao = new Dao<>(marsEntityManager, Employee.class);
        Dao<Employee> saturnEmployeeDao = new Dao<>(saturnEntityManager, Employee.class);

        Employee marsEmployee = createEmployee("Jan Mars", "Programmer");
        Employee saturnEmployee = createEmployee("Adam Saturn", "Manager");

        saturnEmployeeDao.save(saturnEmployee);
        marsEmployeeDao.save(marsEmployee);

        List<Employee> allMars = marsEmployeeDao.all();
        List<Employee> allSaturn = saturnEmployeeDao.all();

        assertThat(allMars).containsOnly(marsEmployee);
        assertThat(allSaturn).containsOnly(saturnEmployee);
    }

}
