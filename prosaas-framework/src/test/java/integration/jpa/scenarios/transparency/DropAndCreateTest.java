package integration.jpa.scenarios.transparency;

import integration.backend.templates.MockedTenancyTest;
import integration.jpa.dao.Dao;
import integration.jpa.data.DataCreator;
import integration.jpa.model.Employee;
import integration.jpa.model.Shared;
import org.fest.assertions.Assertions;
import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;

public class DropAndCreateTest extends MockedTenancyTest {
    @Test
    public void shouldNotDropDatabaseWhenCreatingNewFactory() {
        EntityManager crossTenantEntityManager = JpaStorage.getCrossTenantEntityManager();
        Dao<Shared> sharedDao = new Dao<>(crossTenantEntityManager, Shared.class);
        Shared shared = DataCreator.createShared("some data");
        sharedDao.save(shared);

        mockTenantId("the tenant");
        EntityManager tenantAwareEntityManager = JpaStorage.getTenantAwareEntityManager();
        Dao<Employee> employeeDao = new Dao<>(tenantAwareEntityManager, Employee.class);
        Employee employee = DataCreator.createEmployee("Jan Kowalski", "manager");
        employeeDao.save(employee);

        Assertions.assertThat(sharedDao.all()).contains(shared);
    }
}
