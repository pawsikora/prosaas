package integration.jpa.scenarios.transparency;

import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;

import static integration.jpa.TestConstants.*;

public class SecondaryUnitFactoriesTest extends SpacesTransparencyTest {
    @Test
    public void testTransparencyOnSecondaryUnitWithFactories() {
        mockTenantId(SATURN_TENANT_ID);
        EntityManager saturnEntityManager = JpaStorage.getTenantAwareEntityManagerFactory(SECONDARY_UNIT).createEntityManager();

        mockTenantId(MARS_TENANT_ID);
        EntityManager marsEntityManager = JpaStorage.getTenantAwareEntityManagerFactory(SECONDARY_UNIT).createEntityManager();

        testTransparency(marsEntityManager, saturnEntityManager);
    }
}
