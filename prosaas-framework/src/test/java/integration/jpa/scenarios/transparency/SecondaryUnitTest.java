package integration.jpa.scenarios.transparency;

import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;

import static integration.jpa.TestConstants.MARS_TENANT_ID;
import static integration.jpa.TestConstants.SATURN_TENANT_ID;
import static integration.jpa.TestConstants.SECONDARY_UNIT;

public class SecondaryUnitTest extends SpacesTransparencyTest {
    @Test
    public void testTransparencyOnSecondaryUnit() {
        mockTenantId(SATURN_TENANT_ID);
        EntityManager saturnEntityManager = JpaStorage.getTenantAwareEntityManager(SECONDARY_UNIT);

        mockTenantId(MARS_TENANT_ID);
        EntityManager marsEntityManager = JpaStorage.getTenantAwareEntityManager(SECONDARY_UNIT);

        testTransparency(marsEntityManager, saturnEntityManager);
    }
}
