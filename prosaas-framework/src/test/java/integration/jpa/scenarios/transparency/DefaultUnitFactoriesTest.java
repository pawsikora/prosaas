package integration.jpa.scenarios.transparency;

import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;

import static integration.jpa.TestConstants.MARS_TENANT_ID;
import static integration.jpa.TestConstants.SATURN_TENANT_ID;

public class DefaultUnitFactoriesTest extends SpacesTransparencyTest {
    @Test
    public void testTransparencyOnDefaultUnitWithFactories() {
        mockTenantId(SATURN_TENANT_ID);
        EntityManager saturnEntityManager = JpaStorage.getTenantAwareEntityManagerFactory().createEntityManager();

        mockTenantId(MARS_TENANT_ID);
        EntityManager marsEntityManager = JpaStorage.getTenantAwareEntityManagerFactory().createEntityManager();

        testTransparency(marsEntityManager, saturnEntityManager);
    }
}
