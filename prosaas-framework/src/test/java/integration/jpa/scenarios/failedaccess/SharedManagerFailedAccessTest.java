package integration.jpa.scenarios.failedaccess;

import integration.backend.templates.MockedTenancyTest;
import integration.jpa.dao.Dao;
import integration.jpa.model.Employee;
import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

public class SharedManagerFailedAccessTest extends MockedTenancyTest {
    @Test(expected = PersistenceException.class)
    public void testSharedManagerFailingWhenAccessingTenantData() {
        EntityManager crossTenantEntityManager = JpaStorage.getCrossTenantEntityManager();
        new Dao<>(crossTenantEntityManager, Employee.class).all();
    }
}
