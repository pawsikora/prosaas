package integration.jpa.scenarios.sharedentities;

import integration.jpa.TestConstants;
import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;

public class ManagersTest extends SharedEntitiesTest {
    @Test
    public void testSharedEntitiesWithManagers() {
        mockTenantId(TestConstants.MARS_TENANT_ID);
        EntityManager marsManager = JpaStorage.getTenantAwareEntityManager();

        mockTenantId(TestConstants.SATURN_TENANT_ID);
        EntityManager saturnManager = JpaStorage.getTenantAwareEntityManager();

        EntityManager sharedManager = JpaStorage.getCrossTenantEntityManager();

        testSharedEntities(marsManager, saturnManager, sharedManager);
    }
}
