package integration.jpa.scenarios.sharedentities;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import integration.backend.templates.MockedTenancyTest;
import integration.jpa.dao.Dao;
import integration.jpa.model.Shared;

import javax.persistence.EntityManager;

import static integration.jpa.data.DataCreator.createShared;
import static org.fest.assertions.Assertions.assertThat;

public abstract class SharedEntitiesTest extends MockedTenancyTest {
    protected void testSharedEntities(EntityManager marsManager,
                                      EntityManager saturnManager,
                                      EntityManager sharedManager) {
        Shared marsEntity = createShared("Saturn inserted");
        Shared saturnEntity = createShared("Mars inserted");
        Shared sharedEntity = createShared("Shared inserted");

        Dao<Shared> marsDao = new Dao<>(marsManager, Shared.class);
        Dao<Shared> saturnDao = new Dao<>(saturnManager, Shared.class);
        Dao<Shared> sharedDao = new Dao<>(sharedManager, Shared.class);

        marsDao.save(marsEntity);
        saturnDao.save(saturnEntity);
        sharedDao.save(sharedEntity);

        String marsData = getEntityIdAndData().apply(marsEntity);
        String saturnData = getEntityIdAndData().apply(saturnEntity);
        String sharedData = getEntityIdAndData().apply(sharedEntity);

        assertThat(Lists.transform(marsDao.all(), getEntityIdAndData())).contains(marsData, saturnData, sharedData);
        assertThat(Lists.transform(saturnDao.all(), getEntityIdAndData())).contains(marsData, saturnData, sharedData);
        assertThat(Lists.transform(sharedDao.all(), getEntityIdAndData())).contains(marsData, saturnData, sharedData);
    }

    private Function<Shared, String> getEntityIdAndData() {
        return new Function<Shared, String>() {
            @Override
            public String apply(Shared shared) {
                return shared.getId() + ":" + shared.getData();
            }
        };
    }
}
