package integration.jpa.scenarios.sharedentities;

import integration.jpa.TestConstants;
import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;

public class FactoriesTest extends SharedEntitiesTest {
    @Test
    public void testSharedEntitiesWithManagers() {
        mockTenantId(TestConstants.MARS_TENANT_ID);
        EntityManager marsManager = JpaStorage.getTenantAwareEntityManagerFactory().createEntityManager();

        mockTenantId(TestConstants.SATURN_TENANT_ID);
        EntityManager saturnManager = JpaStorage.getTenantAwareEntityManagerFactory().createEntityManager();

        EntityManager sharedManager = JpaStorage.getCrossTenantEntityManagerFactory().createEntityManager();

        testSharedEntities(marsManager, saturnManager, sharedManager);
    }
}
