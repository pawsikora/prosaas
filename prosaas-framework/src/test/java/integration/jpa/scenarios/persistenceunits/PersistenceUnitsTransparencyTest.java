package integration.jpa.scenarios.persistenceunits;

import integration.backend.templates.MockedTenancyTest;
import integration.jpa.TestConstants;
import integration.jpa.dao.Dao;
import integration.jpa.model.Employee;
import org.junit.Test;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManager;

import static integration.jpa.data.DataCreator.createEmployee;
import static org.fest.assertions.Assertions.assertThat;

public class PersistenceUnitsTransparencyTest extends MockedTenancyTest {
    @Test
    public void testUnitsTransparency() {
        mockTenantId(TestConstants.MARS_TENANT_ID);
        EntityManager defaultManager = JpaStorage.getTenantAwareEntityManager();
        EntityManager secondaryManager = JpaStorage.getTenantAwareEntityManager(TestConstants.SECONDARY_UNIT);

        Employee defaultEntity = createEmployee("Jan Default", "Programmer");
        Employee secondaryEntity = createEmployee("Adam Secondary", "Manager");

        Dao<Employee> defaultDao = new Dao<>(defaultManager, Employee.class);
        Dao<Employee> secondaryDao = new Dao<>(secondaryManager, Employee.class);

        defaultDao.save(defaultEntity);
        secondaryDao.save(secondaryEntity);

        assertThat(defaultDao.all()).containsOnly(defaultEntity);
        assertThat(secondaryDao.all()).containsOnly(secondaryEntity);
    }
}
