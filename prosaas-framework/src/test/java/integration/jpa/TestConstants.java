package integration.jpa;

public class TestConstants {
    public final static String SATURN_TENANT_ID = "saturn";
    public final static String MARS_TENANT_ID = "mars";
    public final static String SECONDARY_UNIT = "secondary";
}
