package integration.spring.model;

import org.springframework.stereotype.Component;
import pl.prosaas.spring.accounting.Feature;

@Component
public class AccountedFeature {
    public static final String FEATURE = "f-a";
    public static final String FEATURE_RETURN = "f-b";

    @Feature(FEATURE)
    public void paidFeature() {
        /*
        nothing to do
         */
    }

    @Feature(FEATURE_RETURN)
    public int paidReturnFeature() {
        return 0;
    }
}
