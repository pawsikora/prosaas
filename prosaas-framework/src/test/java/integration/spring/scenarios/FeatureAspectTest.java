package integration.spring.scenarios;

import integration.backend.templates.MockedTenancyTest;
import integration.spring.model.AccountedFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.prosaas.tenancy.context.TenantContextAbsentException;
import pl.prosaas.tenancy.context.Tenants;
import pl.prosaas.tenancy.repository.FeatureCall;
import pl.prosaas.tenancy.repository.FeatureCallFilter;
import pl.prosaas.tenancy.repository.TenantsRepository;

import java.util.Collection;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static pl.prosaas.tenancy.repository.FeatureCallFilter.ALL;

@ContextConfiguration("/test-config.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class FeatureAspectTest extends MockedTenancyTest {
    private final static String TENANT_A = "tenant-a";
    private final static String TENANT_B = "tenant-b";

    @Autowired
    AccountedFeature feature;

    @Test
    public void shouldSaveFeatureCalls() {
        mockTenantId(TENANT_A);
        int sizeBefore = getAllFeatureCalls().size();
        feature.paidFeature();
        assertThat(getAllFeatureCalls()).hasSize(sizeBefore + 1);
        feature.paidReturnFeature();
        assertThat(getAllFeatureCalls()).hasSize(sizeBefore + 2);
    }

    @Test
    public void shouldGetByFeatureId() {
        mockTenantId(TENANT_A);
        int featuresCalls = getFeatureCalls().size();
        int returnFeaturesCalls = getReturnFeatureCalls().size();

        feature.paidFeature();
        feature.paidReturnFeature();

        assertThat(getFeatureCalls()).hasSize(featuresCalls + 1);
        assertThat(getReturnFeatureCalls()).hasSize(returnFeaturesCalls + 1);
    }

    @Test
    public void shouldGetByDate() throws InterruptedException {
        TenantsRepository repository = Tenants.getTenantsRepository();
        mockTenantId(TENANT_A);

        Date one = new Date();
        Thread.sleep(2000);
        feature.paidFeature();
        feature.paidReturnFeature();
        Thread.sleep(2000);
        Date three = new Date();
        assertThat(repository.getFeatureCalls(new FeatureCallFilter()
                .startingAt(one)
                .endingAt(three)))
                .hasSize(2);
    }

    @Test
    public void shouldGetByTenant() {
        int aCallsCount = getFeatureCallsPerTenant(TENANT_A).size();
        int bCallsCount = getFeatureCallsPerTenant(TENANT_B).size();
        int allFeatures = getFeatureCalls().size();

        mockTenantId(TENANT_A);
        feature.paidFeature();
        mockTenantId(TENANT_B);
        feature.paidFeature();

        assertThat(getFeatureCallsPerTenant(TENANT_A).size()).isEqualTo(aCallsCount + 1);
        assertThat(getFeatureCallsPerTenant(TENANT_B).size()).isEqualTo(bCallsCount + 1);
        assertThat(getFeatureCalls().size()).isEqualTo(allFeatures + 2);
    }

    @Test(expected = TenantContextAbsentException.class)
    public void shouldFailIfNotTenantContext() {
        clearTenantContext();
        feature.paidFeature();
    }

    private Collection<FeatureCall> getFeatureCallsPerTenant(String tenantId) {
        return Tenants.getTenantsRepository().getFeatureCalls(new FeatureCallFilter().withTenantId(tenantId));
    }

    private Collection<FeatureCall> getReturnFeatureCalls() {
        return Tenants.getTenantsRepository().getFeatureCalls(new FeatureCallFilter().withFeatureId(AccountedFeature.FEATURE_RETURN));
    }

    private Collection<FeatureCall> getFeatureCalls() {
        return Tenants.getTenantsRepository().getFeatureCalls(new FeatureCallFilter().withFeatureId(AccountedFeature.FEATURE));
    }

    private Collection<FeatureCall> getAllFeatureCalls() {
        return Tenants.getTenantsRepository().getFeatureCalls(ALL);
    }
}
