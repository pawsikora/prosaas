package integration.spring.scenarios;

import pl.prosaas.spring.accounting.Feature;

public class FeatureFree {
    @Feature("f-r-e-e")
    public void free() {
        System.out.println("doing for free");
    }
}
