package integration.spring.scenarios;

import integration.backend.templates.MockedTenancyTest;
import integration.spring.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.fest.assertions.Assertions.assertThat;

@ContextConfiguration("/test-config.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class AutowireTest extends MockedTenancyTest {
    private static final String TENANT_A = "tenant A";
    private static final String TENANT_B = "tenant B";

    @Autowired
    private TestComponent testComponent;

    @Autowired
    private TestService testService;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private TestController testController;

    @Test
    public void testTenantComponentAutowire() {
        testComponent(testComponent);
    }

    @Test
    public void testTenantServiceAutowire() {
        testComponent(testService);
    }

    @Test
    public void testTenantRepositoryAutowire() {
        testComponent(testRepository);
    }

    @Test
    public void testTenantControllerAutowire() {
        testComponent(testController);
    }

    private void testComponent(TestBean testComponent) {
        mockTenantId(TENANT_A);
        testComponent.setMessage(TENANT_A);
        mockTenantId(TENANT_B);
        testComponent.setMessage(TENANT_B);

        mockTenantId(TENANT_A);
        assertThat(testComponent.getMessage()).isEqualTo(TENANT_A);

        mockTenantId(TENANT_B);
        assertThat(testComponent.getMessage()).isEqualTo(TENANT_B);
    }
}
