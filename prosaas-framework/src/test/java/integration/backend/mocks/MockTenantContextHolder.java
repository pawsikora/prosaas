package integration.backend.mocks;

import pl.prosaas.tenancy.context.TenantContextHolder;

public class MockTenantContextHolder extends TenantContextHolder {

    private TenantContext mockContext;

    @Override
    public TenantContext getCurrentTenantContext() {
        return mockContext;
    }

    public void mockCurrentContext(TenantContext tenantContext) {
        this.mockContext = tenantContext;
    }


}
