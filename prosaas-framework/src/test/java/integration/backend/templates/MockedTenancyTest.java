package integration.backend.templates;

import integration.backend.mocks.MockTenantContextHolder;
import org.junit.Before;
import pl.prosaas.tenancy.context.TenantContextHolder;
import pl.prosaas.tenancy.context.Tenants;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockedTenancyTest {
    private MockTenantContextHolder tenantContextHolder;

    @Before
    public void seuUpTenancyContextHolder() {
        tenantContextHolder = new MockTenantContextHolder();
        Tenants.setTenantContextHolder(tenantContextHolder);
    }

    protected void mockTenantId(String tenantId) {
        TenantContextHolder.TenantContext context = mock(TenantContextHolder.TenantContext.class);
        when(context.getTenantId()).thenReturn(tenantId);
        tenantContextHolder.mockCurrentContext(context);
    }

    protected void clearTenantContext() {
        tenantContextHolder.mockCurrentContext(null);
    }
}
