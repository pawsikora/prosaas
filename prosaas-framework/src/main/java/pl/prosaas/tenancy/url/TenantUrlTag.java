package pl.prosaas.tenancy.url;

import pl.prosaas.tenancy.context.Tenants;

import javax.servlet.jsp.JspTagException;

/**
 * Created by pawel on 21.04.14.
 *
 * Tag which overwrite jstl url tag and adds tenant id to url.
 */
public class TenantUrlTag extends org.apache.taglibs.standard.tag.rt.core.UrlTag{


    @Override
    public void setValue(String value) throws JspTagException {
        String tenant = Tenants.getTenantContextHolder().getCurrentTenantId();
        if(tenant != null){
            super.setValue("/" + tenant + value);
        }else{
            super.setValue(value);
        }
    }
}
