package pl.prosaas.tenancy.url;

import org.apache.http.client.utils.URIBuilder;
import pl.prosaas.tenancy.context.Tenants;
import pl.prosaas.tenancy.repository.TenantException;
import pl.prosaas.tenancy.session.TenantSessionWrapper;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by pawel on 21.04.14.
 *
 * Remove tenant id from url. Set tenantId in thread local variable. Adds tenant id to url in case of redirection.
 */
public class TenantUrlFilter implements Filter {

    private Set<String> ignoredSuffixes = new HashSet<>();
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String[] suffixes = filterConfig.getInitParameter("ignored-suffixes").split(",");
        ignoredSuffixes.addAll(Arrays.asList(suffixes));
    }

    @Override
    //TODO: assert only one execution per request.
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String tenant, newServletPath;
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String path = httpRequest.getServletPath();

        if(ignorePath(path)){ //no tenant given or already set
            chain.doFilter(request, response);
        }else{
            int splitPos = path.indexOf("/", 1);
            if(splitPos == -1){ // homePage
                tenant = path.substring(1);
                newServletPath = "/";
            }else{ //subpage
                tenant = path.substring(1, splitPos);
                newServletPath = path.substring(splitPos);
            }


            try {
                Tenants.getTenantContextHolder().setCurrentTenant(tenant);
            } catch (TenantException e) {
                throw new ServletException("Bad Tenant: " + tenant, e);
            }


            HttpServletResponseWrapper responseWrapper = getHttpServletResponseWrapper(
                    (HttpServletResponse) response,
                    tenant,
                    httpRequest.getContextPath(), httpRequest.getServerName());
            System.out.println("tenant: " + tenant+ ", servletPath: " + newServletPath);
            request.getRequestDispatcher(newServletPath).forward(getHttpServletRequestWrapper(httpRequest, tenant), responseWrapper);
            System.out.println("after forwarding");
        }
    }

    private HttpServletRequestWrapper getHttpServletRequestWrapper(final HttpServletRequest request, final String tenant){
        return new HttpServletRequestWrapper(request){
            @Override
            public HttpSession getSession(boolean create) {
                HttpSession session = super.getSession(create);
                if(session == null)
                    return null;
                return new TenantSessionWrapper(session, tenant, request);
            }

            @Override
            public HttpSession getSession() {
                return getSession(true);
            }

            @Override
            public String getRequestedSessionId() {
                return tenant + super.getRequestedSessionId();
            }


        };
    }

    //TODO: parse whole output response looking for urls.
    private HttpServletResponseWrapper getHttpServletResponseWrapper(final HttpServletResponse response, final String tenant, final String contextPath, final String host) {
        return new HttpServletResponseWrapper(response){
            @Override
            public void sendRedirect(String location) throws IOException {
                location = insertTenantIfNotIgnored(location, host, tenant, contextPath);
                System.out.println("Redirection: " + location);
                super.sendRedirect(location);
            }

            @Override
            public void addCookie(Cookie cookie) {
                String cookiePath = insertTenantIfNotIgnored(cookie.getPath(), host, tenant, contextPath);
                cookie.setPath(cookiePath);
                System.out.println("cookie path: " + cookiePath);
                super.addCookie(cookie);
            }
        };
    }

    private String insertTenantIfNotIgnored(String location, String host, String tenant, String contextPath) {
        if(!isIgnoredResource(location)){
            location = insertTenantId(location, host, tenant, contextPath);
        }
        return location;
    }

    private String insertTenantId(String location, String host, String tenant, String contextPath) {
        try {
            URIBuilder uriBuilder = new URIBuilder(location);
            if(uriBuilder.isAbsolute()){
                if(uriBuilder.getHost().equals(host)){
                    uriBuilder.setPath(putTenantToRelativePath(uriBuilder.getPath(), tenant, contextPath));
                    location = uriBuilder.build().toString();
                }
            }else{
                location = putTenantToRelativePath(location, tenant, contextPath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    private String putTenantToRelativePath(String path, String tenant, String contextPath) {
        String replacement = "/" + tenant;
        if(!contextPath.equals("/")){
            replacement = contextPath + replacement;
        }
        if(path.startsWith("/")){
            path = path.replaceFirst(contextPath, replacement);
        }
        return path;
    }

    private boolean ignorePath(String path) {

        if(path.length() <= 1){
            System.out.println("no tenant in url: " + path);
            return true;
        }

        return isIgnoredResource(path);
    }

    private boolean isIgnoredResource(String path) {
        int lastDot = path.lastIndexOf('.');
        if(lastDot >= 0){
            String suffix = path.substring(lastDot);
            if(ignoredSuffixes.contains(suffix)){
                System.out.println("ignored resource: " + path);
                return true;
            }
        }

        return false;
    }

    @Override
    public void destroy() {

    }
}
