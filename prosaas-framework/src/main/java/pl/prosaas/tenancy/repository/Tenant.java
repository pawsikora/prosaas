package pl.prosaas.tenancy.repository;

import java.util.Date;

public interface Tenant {
    /**
     * @return unique tenant identifier
     */
    String getId();

    /**
     * @return the date when tenant was created
     */
    Date getJoinDate();

    /**
     * shows if the tenant privileged to use the application. For example, it should enable
     * temporary access locking for specific tenant.
     */
    boolean isActive();
}
