package pl.prosaas.tenancy.repository;

import java.util.Date;

public class FeatureCallFilter {
    public final static FeatureCallFilter ALL = new FeatureCallFilter();

    private String featureId;
    private Date startDate;
    private Date endDate;
    private String tenantId;

    public FeatureCallFilter withFeatureId(String featureId) {
        this.featureId = featureId;
        return this;
    }

    public FeatureCallFilter startingAt(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public FeatureCallFilter endingAt(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public FeatureCallFilter withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public String getFeatureId() {
        return featureId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getTenantId() {
        return tenantId;
    }
}
