package pl.prosaas.tenancy.repository;

import java.util.Collection;

public interface TenantsRepository {
    Collection<Tenant> getAllTenants();

    Tenant findTenant(String id) throws TenantException.TenantNotFoundException;

    Tenant addNewTenant(String id) throws TenantException.TenantAlreadyExistsException;

    Tenant activateTenant(String id) throws TenantException.TenantNotFoundException;

    Tenant deactivateTenant(String id) throws TenantException.TenantNotFoundException;

    FeatureCall makeFeatureCall(String featureId);

    Collection<FeatureCall> getFeatureCalls(FeatureCallFilter featureCallFilter);
}
