package pl.prosaas.tenancy.repository;

/**
 * Created by pawel on 03.05.14.
 */
public class TenantException extends Exception{

    public TenantException() {
    }

    public TenantException(String message) {
        super(message);
    }

    public TenantException(String message, Throwable cause) {
        super(message, cause);
    }

    public TenantException(Throwable cause) {
        super(cause);
    }

    public TenantException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public static class TenantNotActiveException extends TenantException{
        public TenantNotActiveException(String tenantId) {
            super("Tenant is not active: " + tenantId);
        }
    }

    public static class TenantAlreadyExistsException extends TenantException {
        public TenantAlreadyExistsException(String tenantId) {
            super("Tenant exists: " + tenantId);
        }
    }

    public static class TenantNotFoundException extends TenantException {
        public TenantNotFoundException(String tenantId) {
            super("Tenant not found: " + tenantId);
        }
    }

}
