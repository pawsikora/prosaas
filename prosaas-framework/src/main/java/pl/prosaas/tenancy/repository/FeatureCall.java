package pl.prosaas.tenancy.repository;

import java.util.Date;

public interface FeatureCall {
    Date getDate();

    String getFeatureId();

    String getTenant();
}
