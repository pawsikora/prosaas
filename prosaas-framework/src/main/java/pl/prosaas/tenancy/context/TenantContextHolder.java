package pl.prosaas.tenancy.context;

import pl.prosaas.tenancy.repository.Tenant;
import pl.prosaas.tenancy.repository.TenantException;


public class TenantContextHolder {

    public static class TenantContext {
        private final String tenantId;

        private TenantContext(String tenantId) {
            this.tenantId = tenantId;
        }

        public String getTenantId() {
            return tenantId;
        }
    }


    private ThreadLocal<TenantContext> tenantContextThreadLocal = new ThreadLocal<>();

    public TenantContext getCurrentTenantContext(){
        return tenantContextThreadLocal.get();
    }

    public String getCurrentTenantId(){
        if(getCurrentTenantContext() != null){
            return getCurrentTenantContext().getTenantId();
        }else{
            return null;
        }
    }

    public void setCurrentTenant(String tenantId) throws TenantException.TenantNotFoundException, TenantException.TenantNotActiveException {
        tenantContextThreadLocal.set(createTenantContext(tenantId));
    }

    public TenantContext createTenantContext(String tenantId) throws TenantException.TenantNotFoundException, TenantException.TenantNotActiveException {
        Tenant tenant = Tenants.getTenantsRepository().findTenant(tenantId);
        if(!tenant.isActive()){
            throw new TenantException.TenantNotActiveException(tenantId);
        }
        return new TenantContext(tenantId);
    }

}
