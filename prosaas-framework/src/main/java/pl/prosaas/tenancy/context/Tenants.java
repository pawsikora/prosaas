package pl.prosaas.tenancy.context;

import pl.prosaas.tenancy.jpa.repository.JpaTenantsRepository;
import pl.prosaas.tenancy.repository.TenantsRepository;

public class Tenants {
    private static TenantContextHolder tenantContextHolder = new TenantContextHolder();
    private static TenantsRepository tenantsRepository = new JpaTenantsRepository();

    public static TenantContextHolder.TenantContext getTenantContext() {
        return tenantContextHolder.getCurrentTenantContext();
    }

    public static TenantContextHolder getTenantContextHolder() {
        return tenantContextHolder;
    }

    public static void setTenantContextHolder(TenantContextHolder tenantContextHolder) {
        Tenants.tenantContextHolder = tenantContextHolder;
    }

    public static TenantsRepository getTenantsRepository() {
        return tenantsRepository;
    }

    public static void setTenantsRepository(TenantsRepository tenantsRepository) {
        Tenants.tenantsRepository = tenantsRepository;
    }

}
