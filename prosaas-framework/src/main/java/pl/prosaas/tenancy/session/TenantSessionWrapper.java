package pl.prosaas.tenancy.session;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by pawel on 27.05.14.
 *
 *
 * Simulates different session for each tenant.
 */
public class TenantSessionWrapper implements HttpSession {

    private static final String TENANT_DATA_ATTRIBUTE = "pl.prosaas.tenant.session";

    private final HttpSession orginalSession;
    private final String tenant;
    private final HttpServletRequest httpServletRequest;

    public TenantSessionWrapper(HttpSession orginalSession, String tenant, HttpServletRequest httpServletRequest) {
        this.orginalSession = orginalSession;
        this.tenant = tenant;
        this.httpServletRequest = httpServletRequest;
        getPerTenantSessionData();
    }


    @Override
    public long getCreationTime() {
        return orginalSession.getCreationTime();
    }

    @Override
    public String getId() {
        String id = tenant + orginalSession.getId();
        System.out.println("session id: " + id);
        return id;
    }

    @Override
    public long getLastAccessedTime() {
        return orginalSession.getLastAccessedTime();
    }

    @Override
    public ServletContext getServletContext() {
        return orginalSession.getServletContext();
    }

    @Override
    public void setMaxInactiveInterval(int interval) {
        orginalSession.setMaxInactiveInterval(interval);
    }

    @Override
    public int getMaxInactiveInterval() {
        return orginalSession.getMaxInactiveInterval();
    }

    @Override
    public HttpSessionContext getSessionContext() {
        throw new NoSuchMethodError("This method is deprecated");
    }

    @Override
    public Object getAttribute(String name) {
        return getPerTenantSessionData().getAttributes().get(name);
    }

    private PerTenantSessionData getPerTenantSessionData() {
        TenantSessionAttribute data = getTenantSessionAttribute();

        PerTenantSessionData tenantData = data.getData().get(tenant);
        if(tenantData == null){
            tenantData = new PerTenantSessionData();
            data.getData().put(tenant, tenantData);
        }
        return tenantData;
    }

    private TenantSessionAttribute getTenantSessionAttribute() {
        TenantSessionAttribute data = (TenantSessionAttribute) orginalSession.getAttribute(TENANT_DATA_ATTRIBUTE);
        if(data == null){
            data = new TenantSessionAttribute();
            orginalSession.setAttribute(TENANT_DATA_ATTRIBUTE, data);
        }
        return data;
    }

    @Override
    public Object getValue(String name) {
        throw new NoSuchMethodError("This method is deprecated");
    }

    @Override
    public Enumeration getAttributeNames() {
        PerTenantSessionData data  = getPerTenantSessionData();
        return new Vector<>(data.getAttributes().keySet()).elements();
    }

    @Override
    public String[] getValueNames() {
        throw new NoSuchMethodError("This method is deprecated");
    }

    @Override
    public void setAttribute(String name, Object value) {
        PerTenantSessionData data = getPerTenantSessionData();
        data.getAttributes().put(name, value);
    }

    @Override
    public void putValue(String name, Object value) {
        throw new NoSuchMethodError("This method is deprecated");
    }

    @Override
    public void removeAttribute(String name) {
        PerTenantSessionData data = getPerTenantSessionData();
        data.getAttributes().remove(name);
    }

    @Override
    public void removeValue(String name) {
        throw new NoSuchMethodError("This method is deprecated");
    }

    @Override
    public void invalidate() {
        TenantSessionAttribute data = getTenantSessionAttribute();
        getPerTenantSessionData().clear();
        data.getData().remove(tenant);
        orginalSession.invalidate();
        httpServletRequest.getSession().setAttribute(TENANT_DATA_ATTRIBUTE, data);
    }

    @Override
    public boolean isNew() {
        return orginalSession.isNew();
    }
}
