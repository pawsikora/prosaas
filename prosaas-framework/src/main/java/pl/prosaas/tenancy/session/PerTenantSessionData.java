package pl.prosaas.tenancy.session;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pawel on 27.05.14.
 *
 * Session data for one tenant.
 */
public class PerTenantSessionData {


    private Map<String, Object> attributes = new HashMap<>();

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void clear() {
        this.attributes.clear();
    }

}
