package pl.prosaas.tenancy.session;

import java.util.HashMap;

/**
 * Created by pawel on 27.05.14.
 *
 * Stores session data for all tenants.
 */
public class TenantSessionAttribute {

    private HashMap<String, PerTenantSessionData> data = new HashMap<>();

    public HashMap<String, PerTenantSessionData> getData() {
        return data;
    }
}
