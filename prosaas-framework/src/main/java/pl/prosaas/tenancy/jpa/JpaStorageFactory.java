package pl.prosaas.tenancy.jpa;

import com.google.common.base.Optional;
import pl.prosaas.tenancy.context.Tenants;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public abstract class JpaStorageFactory {
    private EntityManagerFactoryHolder entityManagerFactoryHolder = new EntityManagerFactoryHolder();

    protected abstract EntityManagerFactory createTenantAwareEntityManagerFactory(String persistenceUnit, String tenantId);

    protected abstract EntityManagerFactory createCrossTenantEntityManagerFactory(String persistenceUnit);

    public EntityManagerFactory getTenantAwareEntityManagerFactory() {
        return getTenantAwareEntityManagerFactory(Constants.DEFAULT_PERSISTENCE_UNIT);
    }

    public EntityManagerFactory getCrossTenantEntityManagerFactory() {
        return getCrossTenantEntityManagerFactory(Constants.DEFAULT_PERSISTENCE_UNIT);
    }

    public synchronized EntityManagerFactory getCrossTenantEntityManagerFactory(String persistenceUnit) {
        Optional<EntityManagerFactory> factoryOptional = entityManagerFactoryHolder
                .getEntityManagerFactory(persistenceUnit);

        if (factoryOptional.isPresent()) {
            return factoryOptional.get();
        }

        EntityManagerFactory factory = createCrossTenantEntityManagerFactory(persistenceUnit);
        entityManagerFactoryHolder.setEntityManagerFactory(persistenceUnit, factory);
        return factory;
    }

    public synchronized EntityManagerFactory getTenantAwareEntityManagerFactory(String persistenceUnit) {
        String tenantId = getTenantId();
        Optional<EntityManagerFactory> factoryOptional = entityManagerFactoryHolder
                .getEntityManagerFactory(persistenceUnit, tenantId);

        if (factoryOptional.isPresent()) {
            return factoryOptional.get();
        }

        EntityManagerFactory factory = createTenantAwareEntityManagerFactory(persistenceUnit, tenantId);
        entityManagerFactoryHolder.setEntityManagerFactory(persistenceUnit, tenantId, factory);
        return factory;
    }

    public EntityManager getCrossTenantEntityManager() {
        return getCrossTenantEntityManagerFactory().createEntityManager();
    }

    public EntityManager getTenantAwareEntityManager() {
        return getTenantAwareEntityManagerFactory().createEntityManager();
    }

    public EntityManager getTenantAwareEntityManager(String persistenceUnit) {
        return getTenantAwareEntityManagerFactory(persistenceUnit).createEntityManager();
    }

    public EntityManager getCrossTenantEntityManager(String persistenceUnit) {
        return getCrossTenantEntityManagerFactory(persistenceUnit).createEntityManager();
    }

    private String getTenantId() {
        return Tenants.getTenantContext().getTenantId();
    }
}
