package pl.prosaas.tenancy.jpa;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;

import javax.persistence.EntityManagerFactory;
import java.util.LinkedList;
import java.util.List;

import static com.google.common.collect.Iterables.tryFind;

public class EntityManagerFactoryHolder {
    private List<PersistenceUnitPool> persistenceUnitPools = new LinkedList<PersistenceUnitPool>();

    public Optional<EntityManagerFactory> getEntityManagerFactory(String persistenceUnit, String tenantId) {
        Optional<PersistenceUnitPool> unitPoolOptional = tryFindPoolWithName(persistenceUnit);
        if (!unitPoolOptional.isPresent()) {
            return Optional.absent();
        }
        return unitPoolOptional.get().getEntityManagerFactory(tenantId);
    }

    public Optional<EntityManagerFactory> getEntityManagerFactory(String persistenceUnit) {
        Optional<PersistenceUnitPool> unitPoolOptional = tryFindPoolWithName(persistenceUnit);
        if (!unitPoolOptional.isPresent()) {
            return Optional.absent();
        }
        return unitPoolOptional.get().getEntityManagerFactory();
    }

    public synchronized void setEntityManagerFactory(String persistenceUnit, String tenantId, EntityManagerFactory factory) {
        getOrCreatePool(persistenceUnit).setEntityManagerFactory(tenantId, factory);
    }

    public synchronized void setEntityManagerFactory(String persistenceUnit, EntityManagerFactory factory) {
        getOrCreatePool(persistenceUnit).setEntityManagerFactory(factory);
    }

    private synchronized PersistenceUnitPool getOrCreatePool(String persistenceUnit) {
        Optional<PersistenceUnitPool> unitPoolOptional = tryFindPoolWithName(persistenceUnit);
        return unitPoolOptional.isPresent() ?
                unitPoolOptional.get() : createPool(persistenceUnit);
    }

    private PersistenceUnitPool createPool(String persistenceUnit) {
        PersistenceUnitPool persistenceUnitPool = new PersistenceUnitPool(persistenceUnit);
        persistenceUnitPools.add(persistenceUnitPool);
        return persistenceUnitPool;
    }

    private Optional<PersistenceUnitPool> tryFindPoolWithName(String persistenceUnit) {
        return tryFind(persistenceUnitPools, nameEquals(persistenceUnit));
    }

    private Predicate<PersistenceUnitPool> nameEquals(final String persistenceUnit) {
        return new Predicate<PersistenceUnitPool>() {
            @Override
            public boolean apply(PersistenceUnitPool persistenceUnitPool) {
                return persistenceUnitPool.getName().equals(persistenceUnit);
            }
        };
    }

    void setPersistenceUnitPools(List<PersistenceUnitPool> persistenceUnitPools) {
        this.persistenceUnitPools = persistenceUnitPools;
    }

    List<PersistenceUnitPool> getPersistenceUnitPools() {
        return persistenceUnitPools;
    }
}
