package pl.prosaas.tenancy.jpa.repository;

import org.eclipse.persistence.annotations.Multitenant;
import org.eclipse.persistence.annotations.MultitenantType;
import pl.prosaas.tenancy.repository.FeatureCall;

import javax.persistence.*;
import java.util.Date;

@Entity
public class FeatureCallEntity implements FeatureCall {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    private Date date;

    private String featureId;

    private String tenant;

    public Long getId() {
        return id;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public String getFeatureId() {
        return featureId;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
}
