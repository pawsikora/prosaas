package pl.prosaas.tenancy.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class JpaStorage {
    private static JpaStorageFactory storageFactory = new EclipselinkJpaStorageFactory();

    public static EntityManager getTenantAwareEntityManager() {
        return storageFactory.getTenantAwareEntityManager();
    }

    public static EntityManager getCrossTenantEntityManager() {
        return storageFactory.getCrossTenantEntityManager();
    }

    public static EntityManagerFactory getTenantAwareEntityManagerFactory() {
        return storageFactory.getTenantAwareEntityManagerFactory();
    }

    public static EntityManagerFactory getCrossTenantEntityManagerFactory() {
        return storageFactory.getCrossTenantEntityManagerFactory();
    }

    public static EntityManager getTenantAwareEntityManager(String persistenceUnit) {
        return storageFactory.getTenantAwareEntityManager(persistenceUnit);
    }

    public static EntityManager getCrossTenantEntityManager(String persistenceUnit) {
        return storageFactory.getCrossTenantEntityManager(persistenceUnit);
    }

    public static EntityManagerFactory getTenantAwareEntityManagerFactory(String persistenceUnit) {
        return storageFactory.getTenantAwareEntityManagerFactory(persistenceUnit);
    }

    public static EntityManagerFactory getCrossTenantEntityManagerFactory(String persistenceUnit) {
        return storageFactory.getCrossTenantEntityManagerFactory(persistenceUnit);
    }
}
