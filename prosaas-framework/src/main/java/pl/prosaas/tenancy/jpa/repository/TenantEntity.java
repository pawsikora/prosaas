package pl.prosaas.tenancy.jpa.repository;

import pl.prosaas.tenancy.repository.Tenant;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
public class TenantEntity implements Tenant {
    @Id
    private String id;
    @Temporal(TemporalType.DATE)
    private Date joinDate;
    private boolean active;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
