package pl.prosaas.tenancy.jpa;

import com.google.common.base.Optional;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class PersistenceUnitPool {
    private final String name;
    private EntityManagerFactory crossTenantEntityManagerFactory;
    private Map<String, EntityManagerFactory> tenantEntityManagerFactories = new HashMap<String, EntityManagerFactory>();

    public PersistenceUnitPool(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Optional<EntityManagerFactory> getEntityManagerFactory(String tenantId) {
        return Optional.fromNullable(tenantEntityManagerFactories.get(tenantId));
    }

    public Optional<EntityManagerFactory> getEntityManagerFactory() {
        return Optional.fromNullable(crossTenantEntityManagerFactory);
    }

    public synchronized void setEntityManagerFactory(String tenantId, EntityManagerFactory factory) {
        tenantEntityManagerFactories.put(tenantId, factory);
    }

    public synchronized void setEntityManagerFactory(EntityManagerFactory factory) {
        crossTenantEntityManagerFactory = factory;
    }
}
