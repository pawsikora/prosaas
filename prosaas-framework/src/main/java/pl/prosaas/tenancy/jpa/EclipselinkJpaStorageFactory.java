package pl.prosaas.tenancy.jpa;

import org.eclipse.persistence.config.PersistenceUnitProperties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * The class creates EntityManagerFactory in accordance with eclipselink properties.
 * Factory methods should be synchronised as they share information in activatedPersistenceUnits.
 */
public class EclipselinkJpaStorageFactory extends JpaStorageFactory {
    private Set<String> activatedPersistenceUnits = new HashSet<>();

    @Override
    protected synchronized EntityManagerFactory createTenantAwareEntityManagerFactory(String persistenceUnit, String tenantId) {
        HashMap<String, String> properties = createPropertiesMap(persistenceUnit);
        properties.put(PersistenceUnitProperties.MULTITENANT_SHARED_EMF, Constants.MULTITENANT_SHARED_EMF);
        properties.put(PersistenceUnitProperties.SESSION_NAME, persistenceUnit + Constants.SESSION_NAME_PREFIX + tenantId);
        properties.put(PersistenceUnitProperties.MULTITENANT_PROPERTY_DEFAULT, tenantId);
        return Persistence.createEntityManagerFactory(persistenceUnit, properties);
    }

    @Override
    protected synchronized EntityManagerFactory createCrossTenantEntityManagerFactory(String persistenceUnit) {
        HashMap<String, String> propertiesMap = createPropertiesMap(persistenceUnit);
        return Persistence.createEntityManagerFactory(persistenceUnit, propertiesMap);
    }

    /**
     * Eclipse Link executes DDL generation every time an EntityManagerFactory is created.
     * Prosaas creates EntityManagerFactories dynamically so we have to prevent DDL generation if executed before.
     *
     * @return initialised properties map
     */
    private HashMap<String, String> createPropertiesMap(String persistenceUnit) {
        HashMap<String, String> properties = new HashMap<>();
        if (activatedPersistenceUnits.contains(persistenceUnit)) {
            properties.put(PersistenceUnitProperties.DDL_GENERATION, PersistenceUnitProperties.NONE);
        } else {
            activatedPersistenceUnits.add(persistenceUnit);
        }
        return properties;
    }
}
