package pl.prosaas.tenancy.jpa.repository;

import com.google.common.base.Optional;
import pl.prosaas.tenancy.context.TenantContextAbsentException;
import pl.prosaas.tenancy.context.TenantContextHolder;
import pl.prosaas.tenancy.context.Tenants;
import pl.prosaas.tenancy.jpa.JpaStorage;
import pl.prosaas.tenancy.repository.*;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class JpaTenantsRepository implements TenantsRepository {

    @Override
    public Collection<Tenant> getAllTenants() {
        EntityManager entityManager = getEntityManager();
        CriteriaQuery<TenantEntity> query = entityManager.getCriteriaBuilder().createQuery(TenantEntity.class);
        List<Tenant> returnList = new LinkedList<>();
        returnList.addAll(entityManager.createQuery(query).getResultList());
        entityManager.close();
        return returnList;
    }

    @Override
    public Tenant findTenant(String id) throws TenantException.TenantNotFoundException {
        EntityManager entityManager = getEntityManager();
        Optional<TenantEntity> tenantEntityOptional = tryFindTenant(id, entityManager);
        entityManager.close();
        if (!tenantEntityOptional.isPresent()) {
            throw new TenantException.TenantNotFoundException(id);
        }
        return tenantEntityOptional.get();
    }

    @Override
    public Tenant addNewTenant(String id) throws TenantException.TenantAlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        Optional<TenantEntity> tenantEntityOptional = tryFindTenant(id, entityManager);
        if (tenantEntityOptional.isPresent()) {
            throw new TenantException.TenantAlreadyExistsException(id);
        }
        TenantEntity tenantEntity = createTenantEntity(id);
        entityManager.persist(tenantEntity);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tenantEntity;
    }

    @Override
    public Tenant activateTenant(String id) throws TenantException.TenantNotFoundException {
        return changeTenantActivity(id, true);
    }

    @Override
    public Tenant deactivateTenant(String id) throws TenantException.TenantNotFoundException {
        return changeTenantActivity(id, false);
    }

    @Override
    public FeatureCall makeFeatureCall(String featureId) {
        FeatureCallEntity featureCall = new FeatureCallEntity();
        featureCall.setDate(new Date());
        featureCall.setFeatureId(featureId);
        TenantContextHolder.TenantContext tenantContext = Tenants.getTenantContext();
        if (tenantContext == null) {
            throw new TenantContextAbsentException();
        }
        featureCall.setTenant(tenantContext.getTenantId());

        EntityManager entityManager = JpaStorage.getCrossTenantEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(featureCall);
        entityManager.getTransaction().commit();

        return featureCall;
    }

    @Override
    public Collection<FeatureCall> getFeatureCalls(FeatureCallFilter featureCallFilter) {
        EntityManager entityManager = JpaStorage.getCrossTenantEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<FeatureCallEntity> criteria = builder.createQuery(FeatureCallEntity.class);
        Root<FeatureCallEntity> root = criteria.from(FeatureCallEntity.class);
        criteria.select(root);

        filterFeatureCallQuery(featureCallFilter, builder, root, criteria);

        List<FeatureCall> featureCalls = new LinkedList<>();
        featureCalls.addAll(entityManager.createQuery(criteria).getResultList());
        entityManager.close();
        return featureCalls;
    }

    private void filterFeatureCallQuery(FeatureCallFilter featureCallFilter, CriteriaBuilder builder,
                                        Root<FeatureCallEntity> root, CriteriaQuery<FeatureCallEntity> criteria) {
        if (featureCallFilter.getStartDate() != null) {
            criteria.where(builder.greaterThanOrEqualTo(root.<Date>get("date"), featureCallFilter.getStartDate()));
        }
        if (featureCallFilter.getEndDate() != null) {
            criteria.where(builder.lessThanOrEqualTo(root.<Date>get("date"), featureCallFilter.getEndDate()));
        }
        if (featureCallFilter.getFeatureId() != null) {
            criteria.where(builder.equal(root.get("featureId"), featureCallFilter.getFeatureId()));
        }
        if (featureCallFilter.getTenantId() != null) {
            criteria.where(builder.equal(root.get("tenant"), featureCallFilter.getTenantId()));
        }
    }

    private Tenant changeTenantActivity(String id, boolean newActivity) throws TenantException.TenantNotFoundException {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        Optional<TenantEntity> tenantEntityOptional = tryFindTenant(id, entityManager);
        if (!tenantEntityOptional.isPresent()) {
            throw new TenantException.TenantNotFoundException(id);
        }
        TenantEntity tenantEntity = tenantEntityOptional.get();
        tenantEntity.setActive(newActivity);
        entityManager.merge(tenantEntity);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tenantEntity;
    }

    private EntityManager getEntityManager() {
        return JpaStorage.getCrossTenantEntityManager();
    }

    private Optional<TenantEntity> tryFindTenant(String id, EntityManager entityManager) {
        TenantEntity tenantEntity = entityManager.find(TenantEntity.class, id);
        return Optional.fromNullable(tenantEntity);
    }

    private TenantEntity createTenantEntity(String id) {
        TenantEntity tenantEntity = new TenantEntity();
        tenantEntity.setId(id);
        tenantEntity.setJoinDate(new Date());
        tenantEntity.setActive(true);
        return tenantEntity;
    }
}
