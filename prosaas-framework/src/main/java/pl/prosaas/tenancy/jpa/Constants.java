package pl.prosaas.tenancy.jpa;

public class Constants {
    public final static String DEFAULT_PERSISTENCE_UNIT = "multitenant";
    public final static String MULTITENANT_SHARED_EMF = "false";
    public final static String SESSION_NAME_PREFIX = "non-shared-emf-for-";
}
