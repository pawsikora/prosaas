package pl.prosaas.spring.scan;

import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.stereotype.Component;
import pl.prosaas.spring.scope.OptionalTenantScope;
import pl.prosaas.spring.scope.TenantScope;

import java.util.HashMap;
import java.util.Map;

@Component
public class ScopeConfiguration extends CustomScopeConfigurer {
    public ScopeConfiguration() {
        Map<String, Object> conf = new HashMap<String, Object>();
        conf.put("tenant", new TenantScope());
        conf.put("optionalTenant", new OptionalTenantScope());
        setScopes(conf);
    }
}
