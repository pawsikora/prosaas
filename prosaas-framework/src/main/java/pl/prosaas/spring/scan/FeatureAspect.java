package pl.prosaas.spring.scan;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import pl.prosaas.spring.accounting.Feature;
import pl.prosaas.tenancy.context.Tenants;

@Aspect
@Component
@EnableAspectJAutoProxy
public class FeatureAspect {

    @AfterReturning(pointcut = "@annotation(pl.prosaas.spring.accounting.Feature)")
    public void logFeature(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Feature feature = signature.getMethod().getAnnotation(Feature.class);
        Tenants.getTenantsRepository().makeFeatureCall(feature.value());
    }
}
