package pl.prosaas.spring.scan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import pl.prosaas.tenancy.context.TenantContextHolder;
import pl.prosaas.tenancy.context.Tenants;
import pl.prosaas.tenancy.jpa.JpaStorage;

import javax.persistence.EntityManagerFactory;

@Configuration
public class JpaConfiguration {
    @Bean(name = "prosaasOptionalTenantEntityManagerFactory")
    @Scope(value = "optionalTenant", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public EntityManagerFactory entityManagerFactory() {
        TenantContextHolder.TenantContext tenantContext = Tenants.getTenantContext();
        if (tenantContext == null) {
            return JpaStorage.getCrossTenantEntityManagerFactory();
        } else {
            return JpaStorage.getTenantAwareEntityManagerFactory();
        }
    }
}
