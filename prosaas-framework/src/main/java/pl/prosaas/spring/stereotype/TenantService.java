package pl.prosaas.spring.stereotype;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Service
@Scope(value = "tenant", proxyMode = ScopedProxyMode.TARGET_CLASS)
public @interface TenantService {
    String value() default "";
}
