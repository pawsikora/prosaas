package pl.prosaas.spring.scope;

import com.google.common.base.Preconditions;
import pl.prosaas.tenancy.context.Tenants;

public class TenantScope extends AbstractTenantScope {

    @Override
    protected String getTenantId() {
        String currentTenantId = Tenants.getTenantContextHolder().getCurrentTenantId();
        Preconditions.checkNotNull(currentTenantId);
        return currentTenantId;
    }

    @Override
    public Object resolveContextualObject(String s) {
        return Tenants.getTenantContext();
    }
}
