package pl.prosaas.spring.scope;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractTenantScope implements Scope {
    Map<String, Map<String, Object>> tenantsBeans = Collections.synchronizedMap(new HashMap<String, Map<String, Object>>());

    @Override
    public Object get(String name, ObjectFactory<?> objectFactory) {
        String id = getTenantId();
        checkTenantPresent(id);
        Map<String, Object> beans = tenantsBeans.get(id);

        synchronized (beans) {
            if (!beans.containsKey(name)) {
                beans.put(name, objectFactory.getObject());
            }
        }
        return beans.get(name);
    }

    @Override
    public Object remove(String s) {
        Map<String, Object> beans = tenantsBeans.get(getTenantId());
        synchronized (beans) {
            return beans.remove(s);
        }
    }

    @Override
    public void registerDestructionCallback(String s, Runnable runnable) {

    }

    @Override
    public String getConversationId() {
        return getTenantId();
    }

    protected abstract String getTenantId();

    private void checkTenantPresent(String tenantId) {
        synchronized (tenantsBeans) {
            if (!tenantsBeans.containsKey(tenantId)) {
                tenantsBeans.put(tenantId, new HashMap<String, Object>());
            }
        }
    }
}
