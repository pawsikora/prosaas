package pl.prosaas.spring.scope;

import com.google.common.base.Optional;
import pl.prosaas.tenancy.context.Tenants;

public class OptionalTenantScope extends AbstractTenantScope {
    /**
     * @return current tenant id or empty string if no tenant present
     */
    @Override
    protected String getTenantId() {
        String currentTenantId = Tenants.getTenantContextHolder().getCurrentTenantId();
        return currentTenantId == null ? "" : currentTenantId;
    }

    @Override
    public Object resolveContextualObject(String key) {
        return Optional.of(Tenants.getTenantContext());
    }
}
