import re

from helpers import Http


writer = {"j_username": "user1", "j_password": "user1"}
reader = {"j_username": "user2", "j_password": "user2"}
init_tenants = ["tenant1", "tenant2", "tenant3", "myTenant"]

http = Http(server_url="http://127.0.0.1:8080")


def home_url(tenant):
    return "/" + tenant


def add_post_url(tenant):
    return "/" + tenant + "/addPost"


def add_comment_url(tenant, post):
    return "/" + tenant + "/post/" + post


def add_tenant_url(tenant):
    return "/" + init_tenants[0] + "/manage/addTenant/" + tenant


def add_post(post_title, tenant):
    http.post(add_post_url(tenant), parameters={"title": post_title, "content": "content of " + post_title})


def add_comment(post_title, tenant):
    http.post(add_comment_url(tenant, post_title), parameters={"content": "comment for " + post_title})


def add_tenant(tenant):
    rsp = http.post(add_tenant_url(tenant), parameters={})


def login(user, tenant):
    response = http.get("/" + tenant)
    pat = re.compile(r'^.*form action="(/' + tenant + r'\S*)"[^>]*>.*$')
    page = str(response.read())
    patterns = re.findall(pat, page)
    assert len(patterns) == 1
    post_link = patterns[0]
    response = http.post(post_link, parameters=user)
    #print(str(response.read()))


if __name__ == "__main__":
    login(writer, init_tenants[0])
    tenant = "test_tenant"
    add_tenant(tenant)
    add_post("post1", tenant)
    add_comment("post1", tenant)