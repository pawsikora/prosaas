#httplib.HTTPConnection.debuglevel = 1
import urllib.request
import urllib.parse
from http.cookiejar import CookieJar


class Http:
    def __init__(self, server_url="", redirect_callback=None):
        self.server_url = server_url
        self.redirect_callback = redirect_callback
        self.cookie_jar = CookieJar()
        self.opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self.cookie_jar))

        urllib.request.install_opener(self.opener)

    def get(self, url, headers={}):
        url = self.server_url + url
        request = urllib.request.Request(url, headers=headers)
        return self.execute_request(request)

    def post(self, url, headers={}, parameters=None):
        url = self.server_url + url
        data = None
        if parameters != None:
            data = urllib.parse.urlencode(parameters)

        request = urllib.request.Request(url, data, headers)
        return self.execute_request(request)

    def execute_request(self, request):
        response = self.opener.open(request)
        # Check for redirect, maybe better way to do this
        '''if response.geturl() != request.get_full_url():
            if self.redirect_callback == None:
                raise "Redirected to '" + response.geturl() + "' but no redirect callback defined"
            else:
                self.redirect_callback(response)'''

        return response