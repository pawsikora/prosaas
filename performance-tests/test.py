import random
import time

import api


class TestData(object):
    def __init__(self):
        self.tenants_posts = {}
        self.tenants_count = 0
        self.posts_count = 0
        api.login(api.writer, api.init_tenants[0])

    def next_tenant(self):
        self.tenants_count += 1
        return "test-tenant" + str(self.tenants_count)

    def next_post(self):
        self.posts_count += 1
        return "post-test-title" + str(self.posts_count)

    def add_tenant(self, tenant):
        api.add_tenant(tenant)
        self.tenants_posts[tenant] = []

    def add_tenants(self, count):
        for i in range(count):
            self.add_next_tenant()

    def add_next_tenant(self):
        tenant = self.next_tenant()
        self.add_tenant(tenant)

    def add_post(self, tenant):
        post = self.next_post()
        if tenant not in self.tenants_posts:
            self.add_tenant(tenant)
        api.add_post(post, tenant)
        self.tenants_posts[tenant].append(post)

    def add_comment(self, post):
        for t, p in self.tenants_posts.items():
            if p == post:
                tenant = t
        api.add_comment(post, tenant)

    def add_comment_for_random_post(self, tenant):
        assert tenant in self.tenants_posts
        post = random.choice(self.tenants_posts[tenant])
        api.add_comment(post, tenant)

    def add_post_for_all(self):
        for tenant in self.tenants_posts.keys():
            self.add_post(tenant)

    def add_comment_for_all(self):
        for tenant in self.tenants_posts.keys():
            self.add_comment_for_random_post(tenant)

    def login_to_all(self):
        for tenant in self.tenants_posts.keys():
            api.login(api.writer, tenant)


if __name__ == "__main__":
    posts_count = 1000
    tenants_to_add = 250
    posts_per_tenant = int(posts_count / tenants_to_add)
    assert posts_per_tenant * tenants_to_add == posts_count

    test_data = TestData()
    test_data.add_tenants(tenants_to_add)
    test_data.login_to_all()
    start = time.time()
    for i in range(posts_per_tenant):
        test_data.add_post_for_all()
        test_data.add_comment_for_all()
        test_data.add_comment_for_all()
        test_data.add_comment_for_all()
        test_data.add_comment_for_all()

    end = time.time()
    print('time: ', end - start)