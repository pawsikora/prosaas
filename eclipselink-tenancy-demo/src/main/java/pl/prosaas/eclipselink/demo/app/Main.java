package pl.prosaas.eclipselink.demo.app;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import pl.prosaas.eclipselink.demo.model.Employee;
import pl.prosaas.eclipselink.demo.model.Shared;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.HashMap;
import java.util.List;

public class Main {
    private static final String SATURN = "Saturn";
    private static final String MARS = "Mars";

    private static EntityManager getTenantAwareEntityManager(String tenantId) {
        HashMap properties = new HashMap();
        properties.put(PersistenceUnitProperties.MULTITENANT_SHARED_EMF, "false");
        properties.put(PersistenceUnitProperties.SESSION_NAME, "non-shared-emf-for-" + tenantId);
        properties.put(PersistenceUnitProperties.MULTITENANT_PROPERTY_DEFAULT, tenantId);
        return Persistence.createEntityManagerFactory("mt-demo", properties).createEntityManager();
    }

    private static EntityManager getEntityManager() {
        return Persistence.createEntityManagerFactory("mt-demo").createEntityManager();
    }

    private static void persistEntity(Object entity, EntityManager entityManager) {
        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
    }

    private static <T extends Object> void listAll(EntityManager entityManager, Class<T> entityClass) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery query = criteriaBuilder.createQuery(entityClass);
        List resultList = entityManager.createQuery(query).getResultList();
        System.out.println(resultList);
    }



    public static void main(String[] args) {
        Employee saturnEmployee = new Employee();
        saturnEmployee.setName("Jan Saturn");
        saturnEmployee.setRole("Manager");

        Employee marsEmployee = new Employee();
        marsEmployee.setName("Adam Mars");
        marsEmployee.setRole("Worker");

        Employee sharedEmployee = new Employee();
        sharedEmployee.setName("Martin X");
        sharedEmployee.setRole("Unemployed");

        Shared marsShared = new Shared();
        marsShared.setData("Will be inserted by Mars");

        Shared saturnShared = new Shared();
        saturnShared.setData("Will be inserted by Saturn");

        Shared shared = new Shared();
        shared.setData("Will be inserted by shared entity manager");

        EntityManager saturnEntityManager = getTenantAwareEntityManager(SATURN);
        EntityManager marsEntityManager = getTenantAwareEntityManager(MARS);
        EntityManager sharedEntityManager = getEntityManager();

        testInsert(marsEmployee, marsShared, marsEntityManager);
        testInsert(saturnEmployee, saturnShared, saturnEntityManager);
        testInsert(sharedEmployee, shared, sharedEntityManager); // can write to tenant tables! - null tenant_id in db

        testList(marsEmployee, marsShared, marsEntityManager);
        testList(saturnEmployee, saturnShared, saturnEntityManager);
        //testList(sharedEmployee, shared, sharedEntityManager); // cannot read from tenant entities
    }

    private static void testInsert(Employee employee, Shared shared, EntityManager entityManager) {
        persistEntity(employee, entityManager);
        persistEntity(shared, entityManager);
    }

    private static void testList(Employee employee, Shared shared, EntityManager entityManager) {
        listAll(entityManager, Shared.class);
        listAll(entityManager, Employee.class);
    }
}
