<%--@elvariable id="global" type="pl.prosaas.example.model.Global"--%>
<%--@elvariable id="_csrf" type="org.springframework.security.web.csrf.DefaultCsrfToken"--%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="ps" uri="http://prosaas.pl/tenancy/url" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<ps:url value="/j_spring_security_check" var="loginAction"/>
<ps:url value="/j_spring_security_logout" var="logoutAction"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${global.pageTitle}</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main.css"/>" />
</head>
<body>
<h1>${global.pageTitle}</h1>


<sec:authorize access="!isAuthenticated()">
    <form action="${loginAction}" method="POST">
        <label for="username">User Name:</label>
        <input id="username" name="j_username" type="text"/>
        <label for="password">Password:</label>
        <input id="password" name="j_password" type="password"/>
        <input type="submit" value="Log In"/>
    </form>
</sec:authorize>
<sec:authorize access="isAuthenticated()">
    <p>Hi ${pageContext.request.remoteUser}</p>
    <form action="${logoutAction}" method="post">
        <input type="submit" value="Log out" />
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</sec:authorize>


<ul>
    <li><a href="<ps:url value="/" />">home</a></li>
    <sec:authorize access="hasRole('ROLE_WRITER')">
        <li><a href="<ps:url value="/addPost" />">add post</a></li>
    </sec:authorize>
</ul>

<tiles:insertAttribute name="body"/>
</body>
</html>

