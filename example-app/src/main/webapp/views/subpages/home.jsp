<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ps" uri="http://prosaas.pl/tenancy/url" %>

<c:if test="${not empty lastCommentedPost}">
    <h1>Recently commented:</h1>

    <h2><a href="<ps:url value="/post/${lastCommentedPost.address}" />">${lastCommentedPost.title}</a></h2>
</c:if>
<h2>Recent posts</h2>
<%--@elvariable id="posts" type="java.util.List"--%>
<c:forEach var="post" items="${posts}">
    <div>
        <h2><a href="<ps:url value="/post/${post.address}" />">${post.title}</a></h2>

        <p>${post.content}</p>
    </div>
</c:forEach>
