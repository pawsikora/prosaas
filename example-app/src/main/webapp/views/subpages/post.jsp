<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ps" uri="http://prosaas.pl/tenancy/url" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%--@elvariable id="post" type="pl.prosaas.example.model.Post"--%>

<ps:url value="/post/${post.address}" var="formAction" />

<div>
    <h2>${post.title}</h2>
    <h5>${post.author}, ${post.date}</h5>

    <p>${post.content}</p>
</div>
<div>
    <h3>Comments</h3>
    <c:forEach var="comment" items="${post.comments}">
        <div>
            <p>${comment.content}</p>
            <h6>${comment.author}, ${comment.date}</h6>
        </div>
    </c:forEach>
</div>

<sec:authorize access="hasRole('ROLE_READER')">
<h2>Add comment</h2>
<form:form modelAttribute="comment" action="${formAction}" method="post">
    <fieldset>
        <legend>Comments Fields</legend>
        <p>
            <form:label for="content" path="content">Content</form:label>
            <form:input path="content"/>
            <form:errors path="content" cssClass="red"/>
        </p>

        <p>
            <input type="submit" value="Send Comment"/>
        </p>

    </fieldset>
</form:form>
</sec:authorize>


