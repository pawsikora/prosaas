<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ps" uri="http://prosaas.pl/tenancy/url" %>

<ps:url value="/addPost" var="addPostAction"/>

<form:form modelAttribute="post" action="${addPostAction}" method="post">
    <fieldset>
        <legend>Post fields</legend>
        <p>
            <form:label for="title" path="content">Title</form:label>
            <form:input path="title"/>
            <form:errors path="content" cssClass="red"/>
        </p>

        <p>
            <form:label for="content" path="content">Content</form:label>
            <form:input path="content"/>
            <form:errors path="content" cssClass="red"/>
        </p>

        <p>
            <input type="submit" value="Send Post"/>
        </p>
    </fieldset>
</form:form>
