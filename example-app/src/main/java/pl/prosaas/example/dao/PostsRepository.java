package pl.prosaas.example.dao;


import org.springframework.data.repository.PagingAndSortingRepository;
import pl.prosaas.example.model.Post;

/**
 * Created by pawsi_000 on 2014-04-05.
 */


public interface PostsRepository extends PagingAndSortingRepository<Post, Long> {

    public Post findByAddress(String address);
}
