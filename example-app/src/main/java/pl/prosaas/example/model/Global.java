package pl.prosaas.example.model;

/**
 * Created by pawsi_000 on 2014-04-04.
 */
public class Global {

    private String pageTitle;

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
}
