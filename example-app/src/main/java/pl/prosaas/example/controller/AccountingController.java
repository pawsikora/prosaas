package pl.prosaas.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.prosaas.tenancy.context.Tenants;
import pl.prosaas.tenancy.repository.FeatureCall;
import pl.prosaas.tenancy.repository.FeatureCallFilter;

import java.util.Collection;

@RestController
public class AccountingController {

    @RequestMapping("/accounting")
    public @ResponseBody Collection<FeatureCall> featureCalls() {
        return Tenants.getTenantsRepository().getFeatureCalls(FeatureCallFilter.ALL);
    }
}
