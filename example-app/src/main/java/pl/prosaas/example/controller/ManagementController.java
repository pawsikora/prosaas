package pl.prosaas.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.prosaas.tenancy.context.Tenants;
import pl.prosaas.tenancy.repository.TenantException;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/manage")
public class ManagementController {
    private final static String STATUS = "status";
    private final static String MESSAGE = "message";

    @RequestMapping(value = "/addTenant/{tenantId}", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> addTenant(@PathVariable String tenantId) {
        Map<String, String> result = new HashMap<>();
        try {
            Tenants.getTenantsRepository().addNewTenant(tenantId);
            result.put(STATUS, "success");
        } catch (TenantException.TenantAlreadyExistsException e) {
            result.put(STATUS, "error");
            result.put(MESSAGE, "tenant already exists");
        }
        return result;
    }
}
