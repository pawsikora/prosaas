package pl.prosaas.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.prosaas.example.components.LastCommented;
import pl.prosaas.example.dao.PostsRepository;
import pl.prosaas.example.model.Comment;
import pl.prosaas.example.model.Global;
import pl.prosaas.example.model.Post;
import pl.prosaas.spring.accounting.Feature;
import pl.prosaas.tenancy.context.Tenants;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@SuppressWarnings("SpringMVCViewInspection")
@Controller
public class HomeController implements InitializingBean {

    public static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private LastCommented lastCommented;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private PostsRepository postsDao;

    @RequestMapping(value = "/")
    public String home(Model model) throws IOException {
        List<Post> posts = postsDao.findAll(new PageRequest(0, 10, Sort.Direction.DESC, "date")).getContent();
        LOGGER.debug("displaying posts: " + posts.size());
        model.addAttribute("posts", posts);
        model.addAttribute("lastCommentedPost", lastCommented.getLastCommentedPost());
        return "home";
    }

    @RequestMapping(value = "/post/{addr}", method = RequestMethod.GET)
    public String displayPost(@PathVariable String addr, Model model) {
        Post post = postsDao.findByAddress(addr);
        model.addAttribute(post);
        Comment comment = new Comment();
        comment.setPost(post);
        model.addAttribute(comment);
        return "post";
    }

    @RequestMapping(value = "/post/{addr}", method = RequestMethod.POST)
    public String saveComment(@PathVariable String addr, Comment comment, Model model, BindingResult result) {
        Post post = postsDao.findByAddress(addr);
        if (result.hasErrors()) {
            model.addAttribute("post", post);
            model.addAttribute("comment", comment);
            return "post";
        }
        comment.setAuthor("comment author");
        comment.setDate(new Date());
        post.getComments().add(comment);
        LOGGER.debug("saving comment: " + comment);
        postsDao.save(post);
        lastCommented.setLastCommentedPost(post);
        LOGGER.debug("redirecting to: " + getPostRedirection(addr));
        return getPostRedirection(addr);
    }

    @RequestMapping(value = "/addPost", method = RequestMethod.GET)
    public String newPostForm(Model model) {
        model.addAttribute(new Post());
        return "addPost";
    }

    @RequestMapping(value = "/addPost", method = RequestMethod.POST)
    @Feature("POST-ADD")
    public String savePost(Post post, Model model, BindingResult result) {
        if (result.hasErrors()) {
            model.addAttribute(post);
            return "addPost";
        }
        post.setAddress(post.getTitle().toLowerCase().replaceAll(" ", "_"));
        post.setDate(new Date());
        post.setAuthor("default author");
        Post savedPost = postsDao.save(post);

        return getPostRedirection(savedPost.getAddress());
    }

    private String getPostRedirection(String postAddress) {
        return "redirect:/post/" + postAddress;
    }

    @ModelAttribute("global")
    public Global getGlobal() {
        Global g = new Global();
        g.setPageTitle(Tenants.getTenantContextHolder().getCurrentTenantId() + " Blog");
        return g;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String[] initTenants = {"tenant1", "tenant2", "tenant3", "myTenant"};
        for (String tenant : initTenants) {
            Tenants.getTenantsRepository().addNewTenant(tenant);
        }
        Tenants.getTenantsRepository().deactivateTenant("tenant3");
    }
}
