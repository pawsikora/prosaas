package pl.prosaas.example.components;

import pl.prosaas.example.model.Post;
import pl.prosaas.spring.stereotype.TenantComponent;

@TenantComponent
public class LastCommented {
    private Post lastCommentedPost;

    public Post getLastCommentedPost() {
        return lastCommentedPost;
    }

    public synchronized void setLastCommentedPost(Post lastCommentedPost) {
        this.lastCommentedPost = lastCommentedPost;
    }
}
